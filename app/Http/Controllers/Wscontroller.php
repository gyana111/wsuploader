<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use MediaWiki\OAuthClient\ClientConfig;
use MediaWiki\OAuthClient\Consumer;
use MediaWiki\OAuthClient\Client;
use GuzzleHttp\Client as GuzzleClient;
use Sunra\PhpSimple\HtmlDomParser;

class Wscontroller extends Controller
{
    protected $client;

    public function __construct()
    {
        $endpoint = 'https://meta.wikimedia.org/w/index.php?title=Special:OAuth';
        $redir = 'https://meta.wikimedia.org/w/index.php?title=Special%3AOAuth%2Fauthorize&';

        //latest proposed key owner only approved
        // $consumerKey =  '332ccf8afea2ab3e65d1263fcc566886';
        // $consumerSecret = '1c578055d41f7f64fe736077836272b8ea3398e5';

        // proposed key for test app (Working set the call back url)
        $consumerKey =  'a226ae7f2d30de5d340b6516c2601383';
        $consumerSecret = '93233166156eba117760eef09b619d725d0c7d3f';

        //old key long url (Working. set call back blank it will return from where it is left)
        // $consumerKey =  'e39fa6f5676edd6737d319c69d8b91fd';
        // $consumerSecret = '7db45f62e0d675462f9cd70bf1fce51db9b74800';

        $conf = new ClientConfig( $endpoint );
        $conf->setRedirURL( $redir );
        $conf->setConsumer( new Consumer( $consumerKey, $consumerSecret ) );

        $this->client = new Client( $conf );

    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $client = $this->client;

        if ($request->session()->has('editToken') && $request->session()->has('accessToken')) {
            $accessToken = $request->session()->get('accessToken');
            $editToken = $request->session()->get('editToken');

            $user = $client->identify( $accessToken );

            if (!$user) {
                return view('ws.index');
            } else {
                return view('ws.index')->withuser($user);
            }
        }
        if (isset($request->oauth_token)) {

            $token = $request->session()->get('token');
            $verifyCode = $request->oauth_verifier;
            
            $accessToken = $client->complete( $token,  $verifyCode );

            $request->session()->put('accessToken', $accessToken);

            // Authenticate the user
            $user = $client->identify( $accessToken );

            // Make an Edit
            $editToken = json_decode( $client->makeOAuthCall(
                $accessToken,
                'https://test.wikipedia.org/w/api.php?action=tokens&format=json'
            ) )->tokens->edittoken;

            $request->session()->put('editToken', $editToken);

            if (!$user) {
                return view('ws.index');
            } else {
                return view('ws.index')->withuser($user);
            }

            // // Do a simple API call
            // echo "Getting user info: ";
            // echo $client->makeOAuthCall(
            //     $accessToken,
            //     'https://test.wikipedia.org/w/api.php?action=query&meta=userinfo&uiprop=rights&format=json'
            // );
        }
        return view('ws.index');
    }

    //authorize the app
    public function authorizeApp(Request $request) 
    {
        $client = $this->client;

        if (isset($request->oauth_token)) {

            $token = $request->session()->get('token');
            $verifyCode = $request->oauth_verifier;
            
            $accessToken = $client->complete( $token,  $verifyCode );

            $request->session()->put('accessToken', $accessToken);

            $editToken = json_decode( $client->makeOAuthCall(
                $accessToken,
                'https://test.wikipedia.org/w/api.php?action=tokens&format=json'
            ) )->tokens->edittoken;

            $request->session()->put('editToken', $editToken);

            return redirect('wiki');
        }

        $client = $this->client;
        $client->setCallback( url('authorize'));
        list( $next, $token ) = $client->initiate();
        $request->session()->put('token', $token);
        return redirect($next);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $dom = HtmlDomParser::file_get_html( $request->url );
        $contents = [];
        $elems = $dom->find(".dc_title");
        foreach ($elems as $element) {
            if($element->class == 'metadataFieldValue dc_title'){
                $contents['title'] = html_entity_decode($element->plaintext);
            }
        }
        $elems = $dom->find(".dc_title");
        foreach ($elems as $element) {
            if($element->class == 'metadataFieldValue dc_title'){
                $contents['title'] = html_entity_decode($element->plaintext);
            }
        }
        // dd($contents);
        $client = $this->client;

        if ($request->session()->has('editToken') && $request->session()->has('accessToken')) {
            $accessToken = $request->session()->get('accessToken');
            $editToken = $request->session()->get('editToken');

            $user = $client->identify( $accessToken );

            if (!$user) {
                return view('ws.index');
            } else {
                $url = $request->url;
                return view('ws.show')->withuser($user)->withContents($contents);
            }

            $apiParams = array(
                'action' => 'edit',
                'title' => 'User_talk:Jnanaranjan_sahu',
                'section' => 'new',
                'summary' => 'Hello World',
                'text' => 'Hi8',
                'token' => $editToken,
                'format' => 'json',
            );

            $client->setExtraParams( $apiParams ); // sign these too

            echo $client->makeOAuthCall(
                $accessToken,
                'https://test.wikipedia.org/w/api.php',
                true,
                $apiParams
            );

            return view('ws.index');
        }
    }
    public function logout(Request $request)
    {
        $client = $this->client;

        if ($request->session()->has('editToken') && $request->session()->has('accessToken')) {
            $accessToken = $request->session()->get('accessToken');
            $editToken = $request->session()->get('editToken');

            $apiParams = array(
                'action' => 'logout',
                'token' => $editToken,
                'format' => 'json',
            );

            $client->setExtraParams( $apiParams ); // sign these too

            $logout =  $client->makeOAuthCall(
                $accessToken,
                'https://test.wikipedia.org/w/api.php',
                true,
                $apiParams
            );

            return redirect('wiki');
            // return view('ws.index');
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
